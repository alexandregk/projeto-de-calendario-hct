<script src="/webdesk/vcXMLRPC.js"></script>

<div id="MyWidget_${instanceId}" class="super-widget wcm-widget-class fluig-style-guide" data-params="MyWidget.instance()">
    <div class="panel panel-primary mb-2" id="hctCalendarioPanel">
        <div class="panel-heading">
            <h1><span class="fluigicon fluigicon-calendar"></span> Calendário HCT</h1>
        </div>
        <div id="addDaytimeBtnCalendario" class="fs-float-right fs-xs-space">
            <button type="button" class="btn btn-success addDaytimeBtnCalendario" title="Adicionar dia de prova">
                <span class="fluigicon fluigicon-plus"></span>
            </button>
        </div>
        <div id="filterDaytimeBtnCalendario" class="fs-float-right fs-xs-space">
            <button type="button" id="callToPopUp" class="btn btn-warning popover-click filterDaytimeBtnCalendario"
                data-toggle="popover" title="Aplicar Filtro">
                <span class="fluigicon fluigicon-search"></span>
            </button>
        </div>
        <div class="panel-body">
            <div id='calendar'></div>
        </div>
        <div class="hct-addDaytime" style="display: none;">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-4 date">
                            <div class="form-group">
                                <label for="dataInicio">* Data:</label>
                                <div class="input-group date" id="dataCadastro">
                                    <input type="text" name="dataProva" id="dataProva" class="form-control data-hora calendarioHCTDataInicio">
                                    <span class="input-group-addon">
                                        <span class="fluigicon-calendar fluigicon"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 hct-reserveDaytime" id="hct-reserveDaytime" style="display: none;">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="hct-nomeUsuario">Nome completo: </label>
                    <input type="text" class="form-control" name="hct-nomeUsuario" id="hct-nomeUsuario" />
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-2">
                    <label for="hct-dataProva">Data: </label>
                    <input type="text" class="form-control" name="hct-dataProva" id="hct-dataProva" />
                </div>
                <div class="form-group col-md-2">
                    <label for="hct-horarioProva">Horário: </label>
                    <select class="form-control" id="horarios" name="horarios">
                        <option hidden selected>Selecione uma opção</option>
                        <option value="13h">13:00 às 14:00</option>
                        <option value="14h">14:00 às 15:00</option>
                        <option value="15h">15:00 às 16:00</option>
                        <option value="16h">16:00 às 17:00</option>
                        <option value="2222">Horário Teste</option>
                    </select>
                </div>
            </div>
            <div class="hct-filterDaytime" id="hct-filterDaytime" style="display: none;">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="campoLabel">Dado do Usuário: </label>
                        <input class="form-control" id="usuarioFilter" type="text" name="usuarioFilter"/>
                        <label for="nomeCampo">Nome do Usuário: </label>
                        <input id="nomeFilter" type="text" name="nomeFilter"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>