<div id="MyWidget_${instanceId}" class="super-widget wcm-widget-class fluig-style-guide"
    data-params="MyWidget.instance()">
    <script src="/webdesk/vcXMLRPC.js"></script>
    <div class="panel panel-primary mb-2" id="hctCalendarioPanel">
        <div class="panel-heading">
            <h1><span class="fluigicon fluigicon-calendar"></span> Calendário HCT</h1>
        </div>
        <div class="panel-body">
            <div id='calendar'></div>
        </div>
        <div class="hctModalCalendario" style="display:none;">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-4 date">
                            <div class="form-group">
                                <label for="dataInicio">* Data Início</label>
                                <div class="input-group date" id="dataCadastro">
                                    <input type="text" name="dataCadastro" id="calendarioHCTDataInicio"
                                        class="form-control data-hora calendarioHCTDataInicio" data-date="">
                                    <span class="input-group-addon">
                                        <span class="fluigicon-calendar fluigicon"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 date">
                            <div class="form-group">
                                <label for="horaInicio">* Hora Início</label>
                                <div class="input-group date" id="horaInicio">
                                    <input type="text" name="horaInicio" class="form-control data-hora calendarioHCTHoraInicio1"
                                        data-toggle="tooltip" id="calendarioHCTHoraInicio1" data-placement="bottom" title="Primeiro Horário">
                                    <span class="input-group-addon">
                                        <span class="fluigicon fluigicon-time"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="horaInicio">* Hora Início</label>
                                <div class="input-group date" id="horaInicio1">
                                    <input type="text" name="horaInicio1" class="form-control data-hora calendarioHCTHoraInicio2"
                                        data-toggle="tooltip" id="calendarioHCTHoraInicio2" data-placement="bottom" title="Segundo Horário">
                                    <span class="input-group-addon">
                                        <span class="fluigicon fluigicon-time"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="horaInicio">* Hora Início</label>
                                <div class="input-group date" id="horaInicio2">
                                    <input type="text" name="horaInicio2" class="form-control data-hora calendarioHCTHoraInicio3"
                                        data-toggle="tooltip" id="calendarioHCTHoraInicio3" data-placement="bottom" title="Terceiro Horário">
                                    <span class="input-group-addon">
                                        <span class="fluigicon fluigicon-time"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="horaInicio">* Hora Início</label>
                                <div class="input-group date" id="horaInicio3">
                                    <input type="text" name="horaInicio3" class="form-control data-hora calendarioHCTHoraInicio4"
                                        data-toggle="tooltip" id="calendarioHCTHoraInicio4" data-placement="bottom" title="Quarto Horário">
                                    <span class="input-group-addon">
                                        <span class="fluigicon fluigicon-time"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 date">
                            <div class="form-group">
                                <label for="horaFim">* Hora Fim</label>
                                <div class="input-group date" id="horaFim">
                                    <input type="text" name="horaFim" class="form-control data-hora calendarioHCTHoraFim1"
                                        data-toggle="tooltip" id="calendarioHCTHoraFim1" data-placement="bottom" title="Primeiro Horário">
                                    <span class="input-group-addon">
                                        <span class="fluigicon fluigicon-time"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="horaFim">* Hora Fim</label>
                                <div class="input-group date" id="horaFim1">
                                    <input type="text" name="horaFim1" class="form-control data-hora calendarioHCTHoraFim2"
                                        data-toggle="tooltip" id="calendarioHCTHoraFim2" data-placement="bottom" title="Segundo Horário">
                                    <span class="input-group-addon">
                                        <span class="fluigicon fluigicon-time"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="horaFim">* Hora Fim</label>
                                <div class="input-group date" id="horaFim2">
                                    <input type="text" name="horaFim2" class="form-control data-hora calendarioHCTHoraFim3"
                                        data-toggle="tooltip" id="calendarioHCTHoraFim3" data-placement="bottom" title="Terceiro Horário">
                                    <span class="input-group-addon">
                                        <span class="fluigicon fluigicon-time"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="horaFim">* Hora Fim</label>
                                <div class="input-group date" id="horaFim3">
                                    <input type="text" name="horaFim3" class="form-control data-hora calendarioHCTHoraFim4"
                                        data-toggle="tooltip" id="calendarioHCTHoraFim4" data-placement="bottom" title="Quarto Horário">
                                    <span class="input-group-addon">
                                        <span class="fluigicon fluigicon-time"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 date">
                            <div class="form-group">
                                <label for="horaFim">* Sala</label>
                                <div class="input-group date" id="sala1">
                                    <input type="text" name="horaFim" class="form-control data-hora calendarioHCTNSala"
                                        data-toggle="tooltip" id="calendarioHCTNSala" data-placement="bottom" title="Primeiro Horário">
                                    <span class="input-group-addon">
                                        <span class="fluigicon fluigicon-time"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
