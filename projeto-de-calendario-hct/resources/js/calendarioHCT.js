var MyWidget = SuperWidget.extend({
    //variáveis da widget
    variavelNumerica: null,
    variavelCaracter: null,

    //método iniciado quando a widget é carregada
    init: function () {

        this.toRenderCalendar();
        var _this = this;

        // addDaytime modal 
        $('.addDaytimeBtnCalendario').click(function () {
            var modal_addDaytime = FLUIGC.modal({
                title: 'Title',
                size: 'large',
                content: $('.hct-addDaytime').html(),
                id: 'fluig-modal',
                actions: [{
                    'label': 'Adicionar data',
                    'bind': 'data-createscheduling',
                }, {
                    'label': 'Fechar',
                    'autoClose': true
                }]
            }, function (err, data) {
                if (err) {
                    // do error handling
                } else {
                    // do something with data
                    // Add data via SOAP
                    $("#fluig-modal").on("click", "[data-createscheduling]", function () {
                        _this.createScheduling();
                    });
                }
            });

            var calendarioHCTDataInicio = FLUIGC.calendar('.calendarioHCTDataInicio');
        });

        // filterDaytime modal
        $('.filterDaytimeBtnCalendario').click(function () {
            var modal_addDaytime = FLUIGC.modal({
                title: 'Title',
                size: 'large',
                content: $('.hct-filterDaytime').html(),
                id: 'fluig-modal',
                actions: [{
                    'label': 'Save',
                    'bind': 'data-deleteScheduling',
                }, {
                    'label': 'Close',
                    'autoClose': true
                }]
            }, function (err, data) {
                if (err) {
                    // do error handling
                } else {
                    // do something with data
                    $("#fluig-modal").on("click", "[data-deletescheduling]", function () {
                        _this.deleteScheduling();
                    });
                }
            });
        });

        // Check if is at home, then add class in thumbCalendar
        var homeName = WCMAPI.pageCode;

        // If isHome, do this
        if (homeName == 'home') {
            $('#MyWidget_46876').addClass('hctWrapHome');
            $('#addDaytimeBtnCalendario').hide();
            $('#filterDaytimeBtnCalendario').hide();
            $('hct-addDaytime').hide();
        }


    },

    //BIND de eventos
    bindings: {
        local: {
            'execute': ['click_executeAction']
        },
        global: {
            'createScheduling': ['data-createscheduling'],
            'deleteScheduling': ['data-deleteScheduling'],
            'updateScheduling': ['data-updateScheduling']
        }
    },

    executeAction: function () {
    },

    // ACRESCENTA A DATA NO DATASET
    createScheduling: function () {
        console.log('Entrou no createScheduling');

        //Template envelope XML     
        $.ajax({
            url: '/calendarioHCT/resources/js/xml/ECMCardService_create.xml',
            async: false,
            type: 'GET',
            datatype: 'xml',
            success: function (xml) {
                _xml = $(xml)
            },
            fail: function (error) {
                console.log(error)
            }
        })

        console.log('passou do ajax');

        var dataProva = $($('[name="dataProva"]')[1]).val();

        //Alterar os valores recuperados na variavel _xml
        _xml.find('companyId').text(WCMAPI.tenantCode);
        _xml.find('parentDocumentId').text(64094);
        _xml.find('username').text('alexandre.giacobo@totvs.com.br');
        _xml.find('password').text('ferrariF40##');
        _xml.find('[name=status]').text('Vazio');
        _xml.find('[name=dataProva]').text(dataProva);

        console.log('passou do xml');

        if (dataProva != "") {
            console.log('Entrou no IF e dataProva é diferente de vazio.');
            //Usar o metodo WCMAPI.Create para chamar o webservice
            WCMAPI.Create({
                url: "/webdesk/ECMCardService?wsdl",
                contentType: "text/xml",
                dataType: "xml",
                data: _xml[0],
                success: function (data) {
                    FLUIGC.toast({
                        title: 'Aviso',
                        message: 'Data cadastrada com sucesso',
                        type: 'success'
                    });
                }
            })
        } else {
            FLUIGC.toast({
                title: 'Aviso',
                message: 'A data não foi cadastrada.',
                type: 'danger'
            });
        }
        $('#fluig-modal').modal('hide');

        window.location.reload(true);
    },

    //REMOVE DO DATA DATASET
    deleteScheduling: function () {

        //Template envelope XML     
        $.ajax({
            url: '/calendarioHCT/resources/js/xml/ECMCardService_delete.xml',
            async: false,
            type: 'GET',
            datatype: 'xml',
            success: function (xml) {
                _xml = $(xml)
            },
            fail: function (error) {
                console.log(error)
            }
        })

        var nome = $($('[name="nomeFilter"]')[1]).val();
        var c1 = DatasetFactory.createConstraint('colleagueName', nome, nome, ConstraintType.MUST);
        var ds = DatasetFactory.getDataset('colleague', null, [c1], null);

        if (ds.values.length < 0) {
            FLUIGC.toast({
                title: 'Aviso',
                message: 'Não existe nenhum usuário com esse nome.',
                type: 'danger'
            });
        }

        var id = ds.values[0]["colleaguePK.colleagueId"];
        var c2 = DatasetFactory.createConstraint('userCode', id, id, ConstraintType.MUST);
        var dsCalendario = DatasetFactory.getDataset('colleague', null, [c2], null);

        if (dsCalendario.values.length < 0) {
            FLUIGC.toast({
                title: 'Aviso',
                message: 'Este usuário não está cadastrado em nenhuma prova.',
                type: 'danger'
            });
        }

        var documentid = dsCalendario.values[0]["documentid"];

        //Alterar os valores recuperados na variavel _xml
        _xml.find('companyId').text(WCMAPI.tenantCode);
        _xml.find('username').text('alexandre.giacobo@totvs.com.br');
        _xml.find('password').text('ferrariF40##');
        _xml.find('cardId').text(documentid);

        //Usar o metodo WCMAPI.Create para chamar o webservice
        WCMAPI.Create({
            url: "/webdesk/ECMCardService?wsdl",
            contentType: "text/xml",
            dataType: "xml",
            data: _xml[0],
            success: function (data) {
                FLUIGC.toast({
                    title: 'Aviso',
                    message: 'Usuário Removido.',
                    type: 'success'
                });
            }
        })
        $('#calendarModal').modal('hide');

        window.location.reload(true);
    },

    updateScheduling: function () {
        console.log('Entrou no createScheduling');

        //Template envelope XML     
        $.ajax({
            url: '/calendarioHCT/resources/js/xml/ECMCardService_update.xml',
            async: false,
            type: 'GET',
            datatype: 'xml',
            success: function (xml) {
                _xml = $(xml)
            },
            fail: function (error) {
                console.log(error)
            }
        })

        console.log('passou do ajax');

        var dataProva = $($('[name="hct-dataProva"]')[1]).val();
        var horarioProva = $($('[name="horarios"]')[1]).val();

        //Alterar os valores recuperados na variavel _xml
        _xml.find('companyId').text(WCMAPI.tenantCode);
        _xml.find('parentDocumentId').text(64094);
        _xml.find('username').text('alexandre.giacobo@totvs.com.br');
        _xml.find('password').text('ferrariF40##');
        _xml.find('[name=status]').text('Aguardando');
        _xml.find('[name=horarios]').text(horarioProva);
        _xml.find('[name=userCode]').text(WCMAPI.userCode);

        console.log('passou do xml');

        if (dataProva != "") {
            console.log('Entrou no IF e dataProva é diferente de vazio.');
            //Usar o metodo WCMAPI.Create para chamar o webservice
            WCMAPI.Create({
                url: "/webdesk/ECMCardService?wsdl",
                contentType: "text/xml",
                dataType: "xml",
                data: _xml[0],
                success: function (data) {
                    FLUIGC.toast({
                        title: 'Aviso',
                        message: 'Aguarde a notificação do RH.',
                        type: 'success'
                    });
                }
            })
        } else {
            FLUIGC.toast({
                title: 'Aviso',
                message: 'A data não foi cadastrada.',
                type: 'danger'
            });
        }
        $('#fluig-modal').modal('hide');

        window.location.reload(true);
    },

    // Show the calendar on the DOM
    toRenderCalendar: function () {
        $('#calendar').fullCalendar({
            contentHeight: 375,
            header: {
                left: 'prev, today',
                center: 'title',
                right: 'next'
            },
            dayClick: function (date) {
                // If home == true, then remove fullCalendar dayClick
                if (WCMAPI.pageCode != 'home') {
                    var modal_addDayclick = FLUIGC.modal({
                        title: 'Agende sua capacitação HCT',
                        size: 'large',
                        content: $('#hct-reserveDaytime').html(),
                        id: 'fluig-modal',
                        actions: [{
                            'label': 'Save',
                            'bind': 'data-updateScheduling',
                        }, {
                            'label': 'Close',
                            'autoClose': true
                        }]
                    }, function (err, data) {
                        if (err) {
                            // do error handling
                        } else {
                            // do something with data
                            // Add data via SOAP
                            $("#fluig-modal").on("click", "[data-updatescheduling]", function () {
                                _this.updateScheduling();
                            });
                        }
                    });

                    $('[name="hct-nomeUsuario"]').eq(1).val(WCMAPI.user).attr('readonly', true);

                    $('[name="hct-dataProva"]').eq(1).val(date.format('DD/MM/YYYY'));

                } else {
                    return false;
                }
                var calendarioHCTData = FLUIGC.calendar('#hct-dataProva');
                console.log("Testando a data de hoje: " + date.format('DD.MM.YYYY'));

                $('.fc-prev-button').on('click', function() {
                    this.toColorDays();
                });

                $('.fc-next-button').on('click', function() {
                    this.toColorDays();
                });
            }
        })
        // Remove a title
        $('.modal-title').hide();

        // Align center calendar, days of week title
        $('th.fc-day-header.fc-widget-header').css('text-align', 'center');

        // Add custom bg-color in every wednessday of the year
        $('td.fc-day.fc-widget-content.fc-wed.fc-past').eq(1).css('background-color', '#45b6da');
        $('td.fc-day.fc-widget-content.fc-wed.fc-past').eq(3).css('background-color', '#45b6da');

        // Add custom bg-color on current day
        $('.fc-today').attr('style', 'background-color:#f6f6f6');

        $('.fc-next-button').addClass('agkPimba');
        $('.fc-prev-button').addClass('agkPimba');

        // Check all days, if is equal the defined datinha, change the background
    },

    toColorDays: function () {
        $('td.fc-day.fc-widget-content').each(function (i, value) {
            var month = Math.floor((Math.random() * 12) + 1);
            var day = Math.floor((Math.random() * 30) + 1);

            var date = '2019-' + month + '-' + day;

            if ($(this).attr('data-date') == date) {
                console.log('Deu bom');
                $(this).css('background-color', 'red');
            }
        });
    }

});